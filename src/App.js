import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import SPObject from './components/SPObject';
import SPFields from './components/SPFields';
import SPLinkedObject from './components/SPLinkedObject';
import PSObject from './components/PSObject';

import Home from './components/Home';

function App() {
  return (
    <Router>
      <div>
        <Route exact path='/' component={Home} />
        <Route exact path='/salesforceToPiano' component={SPObject}/>
        <Route exact path='/salesforceToPiano/fields' component={SPFields}/>
        <Route exact path='/salesforceToPiano/linkedObject' component={SPLinkedObject}/>

        <Route exact path='/pianoToSalesforce' component={PSObject} />
      </div>
    </Router>
  );
}

export default App;
