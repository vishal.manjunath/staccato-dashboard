import React, { useState } from 'react';
import '../App.css';

import Fields from './SPFields';
import FieldsTable from './SPFieldsTable';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 720,
        height: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14,18, 0.1)',
        borderRadius: 5
    }
});

const SPLinkedObject = (props) => {
    const linkedCardStyle = useStyles();

    //linked objects states
    const [sobject, setSobject] = useState('');
    const [idPrefix, setIdPrefix] = useState('');
    const [customFieldPrefix, setCustomFieldPrefix] = useState('');
    const [relationshipToParent, setRelationshipToParent] = useState('oneToMany');
    const [fields, setFields] = useState([]);

    const [isLinkedObjectShown, setLinkedObjectShown] = useState(false);
    const [isFieldsTableShown, setFieldsTableShown] = useState(false);

    const fieldsComp = async (value) => {
        setFields([...fields, value]);
        setFieldsTableShown(true);
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {
            sobject,
            idPrefix,
            customFieldPrefix,
            relationshipToParent,
            fields
        };

        // setLinkedObjects([...linkedObjects, data]);

        setSobject('');
        setIdPrefix('');
        setCustomFieldPrefix('');
        setRelationshipToParent('oneToMany');
        setFields([]);

        setLinkedObjectShown(false);
        setFieldsTableShown(false);
        props.linkedComp(data);

        // alert(JSON.stringify(data));
    };

    const showLinkedObject = (e) => {
        e.preventDefault();

        setLinkedObjectShown(true);
    }

    const buildLinkedCard = () => {
        return (
            <Card className={linkedCardStyle.card} variant='outlined'>
                <CardContent>
                    <h3 className='lObjHeader'>linked object</h3>
                    <div className='lObjDiv'>
                        <form>
                            <label className='flabel'>sobject</label>
                            <input 
                                className='linputS'
                                type='text'
                                required
                                value={sobject}
                                onChange={(e) => setSobject(e.target.value)}
                            />
                            <br/>
                            <br/>
                            <label className='flabel'>id prefix</label>
                            <input
                                className='linputID'
                                type='text'
                                required
                                value={idPrefix}
                                onChange={(e)=>setIdPrefix(e.target.value)}
                            />
                            <br/>
                            <br/>
                            <label className='flabel'>custom field prefix</label>
                            <input
                                className='linputCFP'
                                type='text'
                                value={customFieldPrefix}
                                onChange={(e) => setCustomFieldPrefix(e.target.value)}
                            />
                            <br/>
                            <br/>
                            <label className='flabel'>relationship to parent</label>
                            <select
                                className='linputR'
                                value={relationshipToParent}
                                onChange={(e) => setRelationshipToParent(e.target.value)}
                            >
                                <option className='label' value='oneToMany'>One-To-Many</option>
                                <option className='label' value='oneToOne'>One-To-One</option>
                                <option className='label' value='manyToOne'>Many-To-One</option>
                                <option className='label' value='manyToMany'>Many-To-Many</option>
                            </select>
                            <br/>
                            {isFieldsTableShown===true && <FieldsTable fields={fields}/>}
                            <br/>
                            <Fields fieldsComp={fieldsComp}/>
                            <br/>

                            <button className='lSubmit' onClick={handleSubmit}>add</button>
                        </form>
                    </div>
                </CardContent>
            </Card>
        )
    };

    let linkedCard = buildLinkedCard();

    return (
        <div>
            <br/>
            <button className='lSubmit' onClick={showLinkedObject}>add linked object</button>
            <br/>
            {isLinkedObjectShown && linkedCard}
        </div>
    );

}

export default SPLinkedObject;
