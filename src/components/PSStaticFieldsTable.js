import React from 'react';
import '../App.css';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 540,
        height: 'auto',
        marginRight: 'auto',
        marginLeft: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const PSStaticFieldsTable = (props) => {

    const cardStyle = useStyles();

    let data = props.staticFields;

    const buildCard = () => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <br/>
                    <tabel className='table'>
                        <tr>
                            <th>name</th>
                            <th>value</th>
                            <th>datatype</th>
                        </tr>
                        {data.map((item) => 
                        <tr>
                            <td>{item.name}</td>
                            <td>{item.value}</td>
                            <td>{item.dataType}</td>
                        </tr>)}
                    </tabel>
                </CardContent>
            </Card>
        )
    };

    let card = buildCard();

    return (
        <div>
            {card}
        </div>
    )

}

export default PSStaticFieldsTable;
