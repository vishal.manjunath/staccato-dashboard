import React from 'react';
import '../App.css';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 650,
        height: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const FieldsTable = (props) => {
    const tableCardStyle = useStyles();

    let data = props.fields;

    const buildTableCard = () => {
        return (
            <div>
                <h6 className='fieldHeader'>fields</h6>
                <Card className={tableCardStyle.card} variant='outlined'>
                    <CardContent>
                        <table className='table'>
                            <tr>
                                <th>NAME</th>
                                <th>TYPE</th>
                                <th>custom field name</th>
                            </tr>
                            {data.map((item)=> 
                                <tr>
                                    <td>{item.name}</td>
                                    <td>{item.type}</td>
                                    <td>{item.customFieldName}</td>
                                </tr>
                            )}
                        </table>
                    </CardContent>
                </Card>
            </div>
        )
    }

    let tableCard = buildTableCard();

    return (
        <dib>
            {tableCard}
        </dib>
    )
}

export default FieldsTable;