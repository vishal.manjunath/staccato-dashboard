import React, { useState } from 'react';

import SPObject from './SPObject';
import PSObject from './PSObject';

const Home = () => {

    const [salesforceToPiano, setSalesforceToPiano] = useState(undefined);
    const [pianoToSalesforce, setPianoToSalesforce] = useState(undefined);

    const salesforceToPianoComp = async (value) => {
        setSalesforceToPiano(value);
    }

    const pianoToSalesforceComp = async (value) => {
        setPianoToSalesforce(value);
    }

    const handleSub = (e) => {
        e.preventDefault();

        let data = {
            salesforceToPiano,
            pianoToSalesforce
        };

        const json = JSON.stringify(data);
        const element = document.createElement('a');
        const file = new Blob([json], {type: 'text/plain;charset=utf-8'});

        element.href = URL.createObjectURL(file);
        element.download = "config.json";
        document.body.appendChild(element);
        element.click();

        alert(JSON.stringify(data));
        setSalesforceToPiano(undefined);
        setPianoToSalesforce(undefined);
    }

    return (
        <div>
            {/* <form onSubmit={handleSub}> */}
                <h1 className='hH1'>staccato dashboard</h1>
                <SPObject salesforceToPianoComp={salesforceToPianoComp} onChange={(e) => setSalesforceToPiano(e.target.value)}/>
                <br/>
                <PSObject pianoToSalesforceComp={pianoToSalesforceComp}/>
                <br/>
                <button className='hsubmit' onClick={handleSub}>submit</button>
            {/* </form> */}
        </div>
    )
}

export default Home;