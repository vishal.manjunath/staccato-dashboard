import React, { useState } from 'react';
import '../App.css';

import {
    makeStyles, 
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 540,
        height: 'auto',
        marginRight: 'auto',
        marginLeft: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const PSUserFields = (props) => {
    const cardStyle = useStyles();

    const [name, setName] = useState('');
    const [userField, setUserField] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();

        let data = {
            name,
            userField
        };

        props.userFieldsComp(data);
        setName('');
        setUserField('');
    };

    const buildCard = () => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <form>
                        <label className='ulabel'>name</label>
                        <input
                            className='uinputN'
                            type='text'
                            required
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                        <br/>
                        <br/>
                        <label className='ulabel'>user field</label>
                        <select
                            className='uinputD'
                            value={userField}
                            onChange={(e) => setUserField(e.target.value)}
                        >
                            <option className='label' value=''>Select</option>
                            <option className='label' value='firstName'>First Name</option>
                            <option className='label' value='lastName'>Last Name</option>
                            <option className='label' value='email'>Email</option>
                            <option className='label' value='uid'>UID</option>
                        </select>
                        <br/>
                        <br/>
                        <button className='u-OK' onClick={handleSubmit}>ok</button>
                    </form>
                </CardContent>
            </Card>
        )
    }

    let userFieldCard = buildCard();

    return (
        <div>
            {userFieldCard}
        </div>
    )
}

export default PSUserFields;