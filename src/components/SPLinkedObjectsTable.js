import React from 'react';
import '../App.css';
import FieldsTable from './SPFieldsTable';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 700,
        height: 'auto',
        marginRight: 'auto',
        marginLeft: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
})

const LinkedObjectTable = (props) => {
    const tableCardStyle = useStyles();

    let data = props.linkedObjects;

    let fields = true;

    if(data.fields === []) {
        fields = false;
    }

    const buildShowLinkedObjectsCard = (linkedObject) => {
        return (
            <Card className={tableCardStyle.card} variant='outlined'>
                <CardContent>
                    <h3 className='lObjHeader'>linked object</h3>
                    <div className='lObjDiv'>
                        <form>
                            <label className='flabel'>sobject</label>
                            <input 
                                className='linputS'
                                type='text'
                                required
                                value={linkedObject.sobject}
                            />
                            <br/>
                            <br/>
                            <label className='flabel'>id prefix</label>
                            <input
                                className='linputID'
                                type='text'
                                required
                                value={linkedObject.idPrefix}
                            />
                            <br/>
                            <br/>
                            <label className='flabel'>custom field prefix</label>
                            <input
                                className='linputCFP'
                                type='text'
                                required
                                value={linkedObject.customFieldPrefix}
                            />
                            <br/>
                            <br/>
                            <label className='flabel'>relationship to parent</label>
                            <input 
                                className='linputR'
                                type='text'
                                required
                                value={linkedObject.relationshipToParent}
                            />
                            <br/>
                            <br/>
                            {fields === true && <FieldsTable fields={linkedObject.fields}/>}
                        </form>
                    </div>
                </CardContent>
            </Card>
        )
    }

    let showLinkedObjectsCard;

    showLinkedObjectsCard = data.map((linkedObj) => {
        return buildShowLinkedObjectsCard(linkedObj);
    });

    return (
        <div>
            {showLinkedObjectsCard}
        </div>
    )
}

export default LinkedObjectTable;
