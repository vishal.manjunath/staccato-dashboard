import React from 'react';
import '../App.css';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 540,
        height: 'auto',
        marginRight: 'auto',
        marginLeft: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const PSUserFieldsTable = (props) => {
    const cardStyle = useStyles();

    let userFields = props.userFields

    const buildCard = () => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <table className='table'>
                        <tr>
                            <th>name</th>
                            <th>user field</th>
                        </tr>
                        {userFields.map((item) => 
                        <tr>
                            <td>{item.name}</td>
                            <td>{item.userField}</td>
                        </tr>
                        )}
                    </table>
                </CardContent>
            </Card>
        )
    };

    let card = buildCard();

    return (
        <div>
            {card}
        </div>
    );
}

export default PSUserFieldsTable;
