import React, { useState } from 'react';
import '../App.css';

import UserFields from './PSUserFields';
import UserFieldsTable from './PSUserFieldsTable';

import CustomFields from './PSCustomFields';
import CustomFieldsTable from './PSCustomFieldsTable';

import StaticFields from './PSStaticFields';
import StaticFieldsTable from './PSStaticFieldsTable';

import {
    makeStyles,
    Card, 
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 700,
        height: 'auto',
        marginRight: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const PSCreationFields = (props) => {
    const cardStyle = useStyles();

    const [userFields, setUserFields] = useState([]);
    const [customFields, setCustomFields] = useState([]);
    const [staticFields, setStaticFields] = useState([]);

    const [isUserFieldTableShown, setUserFieldTableShown] = useState(false);
    const [isUserFieldShown, setUserFieldShown] = useState(false);

    const [isCustomFieldTableShown, setCustomFieldTableShown] = useState(false);
    const [isCustomFieldShown, setCustomFieldShown] = useState(false);

    const [isStaticFieldTableShown, setStaticFieldTableShown] = useState(false);
    const [isStaticFieldShown, setStaticFieldShown] = useState(false);
 
    const userFieldsComp = async (value) => {
        setUserFields([...userFields, value]);
        setUserFieldShown(false);
        setUserFieldTableShown(true);
    }

    const showUserField = (e) => {
        e.preventDefault();

        setUserFieldShown(true);
    }

    const customFieldsComp = async (value) => {
        setCustomFields([...customFields, value]);
        setCustomFieldShown(false);
        setCustomFieldTableShown(true);
    }

    const showCustomField = (e) => {
        e.preventDefault();

        setCustomFieldShown(true);
    }

    const staticFieldsComp = async (value) => {
        setStaticFields([...staticFields, value]);
        setStaticFieldShown(false);
        setStaticFieldTableShown(true);
    }

    const showStaticField = (e) => {
        e.preventDefault();

        setStaticFieldShown(true);
    }

    const addCreationFeidls =(e) => {
        e.preventDefault();

        let data = {
            userFields,
            customFields,
            staticFields
        };

        props.creationFieldsComp(data);

        // setUserFields([]);
        // setCustomFields([]);
        // setStaticFields([]);
    }

    const buildCreateCard = () => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <form>
                        <h3 className='creationHeader'>creation fields</h3>
                        <h6 className='userHeader'>user fields</h6>
                        <br/>
                        {isUserFieldTableShown && <UserFieldsTable userFields={userFields}/>}
                        <br/>
                        <button className='uSubmit' onClick={showUserField}>add field</button>
                        <br/>
                        <br/>
                        {isUserFieldShown && <UserFields userFieldsComp={userFieldsComp}/>}
                        <h6 className='userHeader'>custom fields</h6>
                        <br/>
                        {isCustomFieldTableShown && <CustomFieldsTable customFields={customFields}/>}
                        <br/>
                        <button className='uSubmit' onClick={showCustomField}>add field</button>
                        <br/>
                        <br/>
                        {isCustomFieldShown && <CustomFields customFieldsComp={customFieldsComp}/>}
                        <h6 className='userHeader'>static fields</h6>
                        <br/>
                        {isStaticFieldTableShown && <StaticFieldsTable staticFields={staticFields} />}
                        <br/>
                        <button className='uSubmit' onClick={showStaticField}>add field</button>
                        <br/>
                        <br/>
                        {isStaticFieldShown && <StaticFields staticFieldsComp={staticFieldsComp} />}
                        <br/>
                        <button className='submit' onClick={addCreationFeidls}>add</button>
                    </form>
                </CardContent>
            </Card>
        )
    };

    let createCard;

    createCard = buildCreateCard();

    return (
        <div>
            {createCard}
        </div>
    );
}

export default PSCreationFields;