import React from 'react';
import '../App.css';

import UserFieldsTable from './PSUserFieldsTable';
import CustomFieldsTable from './PSCustomFieldsTable';
import StaticFieldsTable from './PSStaticFieldsTable';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 750,
        height: 'auto',
        marginLeft: 'auto',
        marginTop: 50,
        marginRight: 'auto',
        marginBottom: 50,
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const PianoToSalesforce = (props) => {
    const cardStyle = useStyles();

    let data = props.allSobject;

    const buildCard = (obj) => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <div>
                        <form>
                            <label className='plabel'>sobject</label>
                            <input
                                className='pinputS'
                                type='text'
                                value={obj.sobject}
                            />
                            {/* <PSCreationFields creationFields={obj.creationFields} /> */}
                            <h6 className='userHeader'>user fields</h6>
                            <UserFieldsTable userFields={obj.userFields} />
                            <h6 className='userHeader'>custom fields</h6>
                            <CustomFieldsTable customFields={obj.customFields} />
                            <h6 className='userHeader'>static fields</h6>
                            <StaticFieldsTable staticFields={obj.staticFields} />
                        </form>
                    </div>
                </CardContent>
            </Card>
        )
    };

    let card;

    card = data.map((obj) => {
        return buildCard(obj);
    })

    return (
        <div>
            {card}
        </div>
    )
}

export default PianoToSalesforce;
