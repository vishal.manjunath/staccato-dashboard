import React, { useState } from 'react';
import '../App.css';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 540,
        height: 'auto',
        marginRight: 'auto',
        marginLeft: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const PSStaticFields = (props) => {
    const cardStyle = useStyles();

    const [name, setName] = useState('');
    const [value, setValue] = useState('');
    const [dataType, setDataType] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();

        let staticField = {
            name,
            value,
            dataType
        }

        props.staticFieldsComp(staticField);
        
        setName('');
        setValue('');
        setDataType('');

    };

    const buildCard = () => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <form>
                        <label className='ulabel'>name</label>
                        <input
                            className='sinputN'
                            type='text'
                            required
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                        <br/>
                        <br/>
                        <label className='ulabel'>value</label>
                        <input
                            className='sinputV'
                            type='text'
                            required
                            value={value}
                            onChange={(e) => setValue(e.target.value)}
                        />
                        <br/>
                        <br/>
                        <label className='ulabel'>datatype</label>
                        <input
                            className='sinputD'
                            type='text'
                            required
                            value={dataType}
                            onChange={(e) => setDataType(e.target.value)}
                        />
                        <br/>
                        <br/>
                        <button className='u-OK' onClick={handleSubmit}>ok</button>
                    </form>
                </CardContent>
            </Card>
        )
    };

    let card = buildCard();

    return (
        <div>
            {card}
        </div>
    )
};

export default PSStaticFields;