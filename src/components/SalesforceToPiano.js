import React from 'react';
import '../App.css';

import SPFieldsTable from './SPFieldsTable';
import SPLinkedObjectsTable from './SPLinkedObjectsTable';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 750,
        height: 'auto',
        marginLeft: 'auto',
        marginTop: 50,
        marginBottom: 50,
        marginRight: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1)',
        borderRadius: 5
    }
});

const SalesforceToPiano = (props) => {
    const cardStyle = useStyles();

    let data = props.allSObjects;

    const buildCard = (obj) => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <div>
                        <form>
                            <label className='label'>sobject</label>
                            <input 
                                className='inputS'
                                type='text'
                                value={obj.sobject}
                            />
                            <br/>
                            <br/>
                            <label className='label'>id prefix</label>
                            <input
                                className='input'
                                type='text'
                                required
                                value={obj.idPrefix}
                            />
                            <br/>
                            <br/>
                            <label className='label'>custom field prefix</label>
                            <input
                                className='inputCFP'
                                type='text'
                                required
                                value={obj.customFieldPrefix}
                            />
                            <br/>
                            <SPFieldsTable fields={obj.fields} />
                            <br/>
                            <SPLinkedObjectsTable linkedObjects={obj.linkedObjects} />
                        </form>
                    </div>
                </CardContent>
            </Card>
        )
    }    

    let card;

    card = data.map((obj) => {
        return buildCard(obj);
    })

    return (
        <div>
            {card}
        </div>
    )
}

export default SalesforceToPiano;