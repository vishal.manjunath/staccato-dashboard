import React, { useState } from 'react';
import '../App.css';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 650,
        height: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const SPFields = (props) => {

    const fieldCardStyle = useStyles();

    //field states
    const [name, setName] = useState('');
    const [type, setType] = useState('');
    const [customName, setCustomName] = useState('');
    const [excludeField, setExcludeField] = useState(false);
    const [customNameCB, setCustomNameCB] = useState(false);

    const [isFieldShown, setFieldShown] = useState(false);

    let field;

    const handleSubmit = (e) => {
        e.preventDefault();

        let customFieldName = '';
        if (customName === '') {
            customFieldName = 'SFC' + name;
        } else {
            customFieldName = customName;
        }

        field = {
            name, 
            type,
            customFieldName,
            excludeField
        };

        props.fieldsComp(field);

        setName('');
        setType('');
        setCustomName('');
        setCustomNameCB(false);
        setExcludeField(false);
        setFieldShown(false);

        
    }

    const handleCustomNameCB = (e) => {
        if (customNameCB === false) {
            setCustomNameCB(true);
        } else {
            setCustomNameCB(false);
        }
    }

    const showField  = (e) => {
        e.preventDefault();

        setFieldShown(true);
    }

    const buildFieldCard = () => {
        return (
            <Card className={fieldCardStyle.card} variant='outlined'>
                <CardContent>
                    <div className='container'>
                        <form>
                            <label className='flabel'>name</label>
                            <input
                                className='finputN'
                                type='text'
                                required
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                            />
                            <br/>
                            <br/>
                            <label className='flabel'>type</label>
                            <input
                                className='finputT'
                                type='text'
                                required
                                value={type}
                                onChange={(e) => setType(e.target.value)}
                            />
                            <br/>
                            <br/>
                            <label className='flabel'>custom field name</label>
                            {customNameCB === true ?
                                (
                                    <input
                                        className='finputP'
                                        type='text'
                                        required
                                        value={customName}
                                        onChange={(e) => setCustomName(e.target.value)}
                                    />
                                ) : (
                                    <input
                                        className='finputP'
                                        type='text'
                                        disabled
                                    />
                                )}
                            <input
                                className='cbCustom'
                                type='checkbox'
                                onClick={handleCustomNameCB}
                            />
                            <label className='cbLabel'>custom name</label>
                            <br/>
                            <br/>
                            <input 
                                className='cbExclude'
                                type ='checkbox'
                                value='true'
                                onChange={(e) => setExcludeField(e.target.value)}
                            />
                            <label className='cbLabel'>exclude field</label>
                            <button className='f-OK' onClick={handleSubmit}>ok</button>
                        </form>
                    </div>
                </CardContent>
            </Card>
        )
    };

    let fieldCard = buildFieldCard();

    return (
        <div>
            <br/>
            <button className='fSubmit' onClick={showField}>add field</button>
            <br/>
            <br/>
            {isFieldShown && fieldCard}
        </div>
    )

};

export default SPFields;