import React, { useState } from 'react';
import '../App.css';

import PSCreationFields from './PSCreationFields';

import UserFields from './PSUserFields';
import UserFieldsTable from './PSUserFieldsTable';

import CustomFields from './PSCustomFields';
import CustomFieldsTable from './PSCustomFieldsTable';

import StaticFields from './PSStaticFields';
import StaticFieldsTable from './PSStaticFieldsTable';

import PianoToSalesforce from './PianoToSalesforce';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 750,
        height: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 50,
        marginBottom: 50,
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const PSObject = (props) => {

    const cardStyle = useStyles();

    const [allSobject, setAllSobjects] = useState([]);
    const [showCard, setShowCard] = useState(false);

    //main sobject
    const [sobject, setSobject] = useState('');
    const [creationFields, setCreationFields] = useState();
    const [userFields, setUserFields] = useState([]);
    const [customFields, setCustomFields] = useState([]);
    const [staticFields, setStaticFields] = useState([]);

    const [create, setCreate] = useState(false);

    const [isUserFieldTableShown, setUserFieldTableShown] = useState(false);
    const [isUserFieldShown, setUserFieldShown] = useState(false);

    const [isCustomFieldTableShown, setCustomFieldTableShown] = useState(false);
    const [isCustomFieldShown, setCustomFieldShown] = useState(false);

    const [isStaticFieldTableShown, setStaticFieldTableShown] = useState(false);
    const [isStaticFieldShown, setStaticFieldShown] = useState(false);

    const userFieldsComp = async (value) => {
        setUserFields([...userFields, value]);
        setUserFieldShown(false);
        setUserFieldTableShown(true);
    }

    const showUserField = (e) => {
        e.preventDefault();

        setUserFieldShown(true);
    }

    const customFieldsComp = async (value) => {
        setCustomFields([...customFields, value]);
        setCustomFieldShown(false);
        setCustomFieldTableShown(true);
    }

    const showCustomField = (e) => {
        e.preventDefault();

        setCustomFieldShown(true);
    }

    const staticFieldsComp = async (value) => {
        setStaticFields([...staticFields, value]);
        setStaticFieldShown(false);
        setStaticFieldTableShown(true);
    }

    const showStaticField = (e) => {
        e.preventDefault();
        setStaticFieldShown(true);
    }

    const creationFieldsComp = async (value) => {
        setCreationFields(value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        let data = {
            sobject,
            creationFields,
            userFields,
            customFields,
            staticFields
        }

        setAllSobjects([...allSobject, data]);
        props.pianoToSalesforceComp(allSobject);
        setShowCard(false);
        setSobject('');
        setUserFields([]);
        setCustomFields([]);
        setStaticFields([]);
    };

    const showBuildCard = (e) => {
        e.preventDefault();

        setShowCard(true);
    }

    const createCheck = (e) => {
        if(create===false) {
            setCreate(true);
        } else {
            setCreate(false);
        }
    }
    const buildCard = () => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <div>
                        <form onSubmit={handleSubmit}>
                            <label className='plabel'>sobject</label>
                            <input  
                                className='pinputS'
                                type='text'
                                required
                                value={sobject}
                                onChange={(e) => setSobject(e.target.value)}
                            />
                            <input
                                className='cbCreate'
                                type='checkbox'
                                value='true'
                                onChange={createCheck}
                            />
                            <label className='cbLabel'>create</label>
                            <br/>
                            <br/>
                            {create === true && <PSCreationFields creationFieldsComp={creationFieldsComp}/>}
                            <br/>
                            <h6 className='userHeader'>user fields</h6>
                            {isUserFieldTableShown && <UserFieldsTable userFields={userFields} />}
                            <br/>
                            <button className='uSubmit' onClick={showUserField}>add field</button>
                            <br/>
                            <br/>
                            {isUserFieldShown && <UserFields userFieldsComp={userFieldsComp} />}
                            <h6 className='userHeader'>custom fields</h6>
                            <br/>
                            {isCustomFieldTableShown && <CustomFieldsTable customFields={customFields} />}
                            <br/>
                            <button className='uSubmit' onClick={showCustomField}>add field</button>
                            <br/>
                            <br/>
                            {isCustomFieldShown && <CustomFields customFieldsComp={customFieldsComp} />}
                            <h6 className='userHeader'>static fields</h6>
                            <br/>
                            {isStaticFieldTableShown && <StaticFieldsTable staticFields={staticFields} />}
                            <br/>
                            <button className='uSubmit' onClick={showStaticField}>add field</button>
                            <br/>
                            <br/>
                            {isStaticFieldShown && <StaticFields staticFieldsComp={staticFieldsComp} />}
                            <br/>
                            <button className='submit'>add</button>
                        </form>
                    </div>
                </CardContent>
            </Card>
        )
    };

    let card;

    card = buildCard();

    return (
        <div>
            <h3 className='mainH3'>piano to salesforce</h3>
            <PianoToSalesforce allSobject={allSobject} />
            <button className='mSubmit' onClick={showBuildCard}>add sobject</button>
            {showCard && card}
        </div>
    );
}

export default PSObject;