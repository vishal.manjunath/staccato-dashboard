import React from 'react';
import '../App.css';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 540,
        height: 'auto',
        marginRight: 'auto',
        marginLeft: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const PSCustomFieldsTable = (props) => {
    const cardStyle = useStyles();

    let data = props.customFields;

    const buildCard = () => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <table className='table'>
                        <tr>
                            <th>custom field name</th>
                            <th>field name</th>
                            <th>title</th>
                            <th>datatype</th>
                            <th>default value</th>
                        </tr>
                        {data.map((item) => 
                        <tr>
                            <td>{item.customFieldName}</td>
                            <td>{item.fieldName}</td>
                            <td>{item.title}</td>
                            <td>{item.dataType}</td>
                            <td>{item.defaultValue}</td>
                        </tr>    
                        )}
                    </table>
                </CardContent>
            </Card>
        )
    };

    let card = buildCard();

    return (
        <div>
            {card}
        </div>
    )
};

export default PSCustomFieldsTable;