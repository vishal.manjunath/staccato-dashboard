import React, { useState } from 'react';
import '../App.css';

import SalesforceToPiano from './SalesforceToPiano';
import Fields from './SPFields';
import LinkedObject from './SPLinkedObject';
import FieldsTable from './SPFieldsTable';
import LinkedObjectsTable from './SPLinkedObjectsTable';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 750,
        height: 'auto',
        marginLeft: 'auto',
        marginTop: 50,
        marginRight: 'auto',
        marginBottom: 50,
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1)',
        borderRadius: 5
    }
});

const SPObject = (props) => {

    const mainCardStyle = useStyles();

    const [allSObjects, setAllSObjects] = useState([]);
    const [showCard, setShowCard] = useState(false);

    //main object states
    const [sobject, setSobject] = useState('');
    const [idPrefix, setIdPrefix] = useState('');
    const [customFieldPrefix, setCustomFieldPrefix] = useState('');
    const [fields, setFields] = useState([]);
    const [linkedObjects, setLinkedObjects] = useState([]);

    const [isFieldsTableShown, setFieldsTableShown] = useState(false);

    const fieldsComp = async (value) => {
        setFields([...fields, value]);
        setFieldsTableShown(true);
    }

    const linkedComp = async (value) => {
        setLinkedObjects([...linkedObjects, value]);
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {
            sobject,
            idPrefix,
            customFieldPrefix,
            fields,
            linkedObjects
        };

        setAllSObjects([...allSObjects, data]);
        
        setSobject('');
        setIdPrefix('');
        setCustomFieldPrefix('');
        setFields([]);
        setLinkedObjects([]);

        setFieldsTableShown(false);
        setShowCard(false);

        props.salesforceToPianoComp(allSObjects);
        console.log(allSObjects);
    }


    const showBuildCard = (e) => {
        e.preventDefault();

        setShowCard(true);
    }

    const buildMainCard = () => {
        return (
            <Card className={mainCardStyle.card} variant='outlined'>
                <CardContent>
                    <div>
                        <form onSubmit={handleSubmit}>
                            <label className='label'>sobject</label>
                            <input
                                className='inputS'
                                type='text'
                                required
                                value={sobject}
                                onChange={(e => setSobject(e.target.value))}
                            />
                            <br/>
                            <br/>
                            <label className='label'>id prefix</label>
                            <input 
                                className='input'
                                type='text'
                                required
                                value={idPrefix}
                                onChange={(e) => setIdPrefix(e.target.value)}
                            />
                            <br/>
                            <br/>
                            <label className='label'>custom field prefix</label>
                            <input
                                className='inputCFP'
                                type='text'
                                required
                                value={customFieldPrefix}
                                onChange={(e) => setCustomFieldPrefix(e.target.value)}
                            />
                            <br/>
                            <br/>
                            {isFieldsTableShown===true && <FieldsTable fields={fields}/>}
                            <Fields fieldsComp={fieldsComp}/>
                            <LinkedObjectsTable linkedObjects={linkedObjects}/>
                            <LinkedObject linkedComp={linkedComp}/>
                            <button className='submit'>add</button>
                        </form>
                    </div>
                </CardContent>
            </Card>
        )
    };

    let mainCard = buildMainCard();

    return (
        <div>
            <h3 className='mainH3'>salesforce to piano</h3>
            <SalesforceToPiano allSObjects={allSObjects}/>
            <button className='mSubmit' onClick={showBuildCard}>add sobject</button>
            {showCard && mainCard}
        </div>
    )

}

export default SPObject;