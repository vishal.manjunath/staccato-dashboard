import React, { useState } from 'react';
import '../App.css';

import {
    makeStyles,
    Card,
    CardContent
} from '@material-ui/core';

const useStyles = makeStyles({
    card: {
        maxWidth: 540,
        height: 'auto',
        marginRight: 'auto',
        marginLeft: 'auto',
        background: '#FFFFFF',
        boxShadow: '0px 15px 40px rgba(10, 14, 18, 0.1);',
        borderRadius: 5
    }
});

const PSCustomFields = (props) => {
    const cardStyle = useStyles();

    const [customFieldName, setCustomFieldName] = useState('');
    const [fieldName, setFieldName] = useState('');
    const [title, setTitle] = useState('');
    const [dataType, setDataType] = useState('');
    const [defaultValue, setDefaultValue] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();

        let customField = {
            customFieldName,
            fieldName,
            title,
            dataType,
            defaultValue
        };

        props.customFieldsComp(customField);
        
        setCustomFieldName('');
        setFieldName('');
        setTitle('');
        setDataType('');
        setDefaultValue('');

        
    }

    const buildCard = () => {
        return (
            <Card className={cardStyle.card} variant='outlined'>
                <CardContent>
                    <form>
                        <label className='ulabel'>custom field name</label>
                        <input 
                            className='cinputC'
                            type='text'
                            required
                            value={customFieldName}
                            onChange={(e) => setCustomFieldName(e.target.value)}
                        />
                        <br/>
                        <br/>
                        <label className='ulabel'>field name</label>
                        <input 
                            className='cinputF'
                            type='text'
                            required
                            value={fieldName}
                            onChange={(e) => setFieldName(e.target.value)}
                        />
                        <br/>
                        <br/>
                        <label className='ulabel'>title</label>
                        <input
                            className='cinputT'
                            type='text'
                            required
                            value={title}
                            onChange={(e) => setTitle(e.target.value)}
                        />
                        <br/>
                        <br/>
                        <label className='ulabel'>datatype</label>
                        <input
                            className='cinputD'
                            type='text'
                            required
                            value={dataType}
                            onChange={(e) => setDataType(e.target.value)}
                        />
                        <br/>
                        <br/>
                        <label className='ulabel'>default value</label>
                        <input
                            className='cinputDV'
                            type='text'
                            required
                            value={defaultValue}
                            onChange={(e) => setDefaultValue(e.target.value)}
                        />
                        <br/>
                        <br/>
                        <button className='u-OK' onClick={handleSubmit}>ok</button>
                    </form>
                </CardContent>
            </Card>
        )
    };

    let card = buildCard();

    return (
        <div>
            {card}
        </div>
    );

}

export default PSCustomFields;
